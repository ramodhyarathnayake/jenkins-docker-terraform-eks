FROM node:18-alpine

RUN mkdir -p /data/app

COPY ./app /data/app

WORKDIR /data/app

CMD ["node", "index.js"]